context('UnitTest - Internal functions')

# Test data setup
data <- matrix(seq(25*100),nrow=25, ncol=100)
pkg.parameters <- list(nsim=100,tstep=25)

test_that('Check vectorization function',{

  result <- VectorizationReturnYoY(data, pkg.parameters)

  expect_that(length(result), equals((pkg.parameters$tstep+1)*pkg.parameters$nsim))
  expect_that(result[1], equals(0))
  expect_that(result[pkg.parameters$tstep+2], equals(0))
  expect_that(result[2], equals(0))
  expect_that(result[3], equals(1))
  expect_that(result[4], equals(0.5))
  expect_that(result[pkg.parameters$tstep+1], equals(25/24-1))
  expect_that(result[pkg.parameters$tstep+3], equals(25))
})

test_that('Check vectorization function for Level',{

  result <- VectorizationLevel(data, 100, pkg.parameters)
  expect_that(length(result), equals((pkg.parameters$tstep+1)*pkg.parameters$nsim))
  expect_that(result[1], equals(100))
  expect_that(result[pkg.parameters$tstep+2], equals(100))
  expect_that(result[2], equals(100))
})
