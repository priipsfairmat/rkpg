# Setup test parameters

pkg.parameters <- list(
  xls.path='W:/1_Group Actuarial Function/Stochastic Scenarios/KID_PRIIPs/_Delivery/InputData/PRIIPs_Input_Template_5ysDaily_20171020.xlsx',
  sf.names=c('GESAV'),
  spread.model.gho=FALSE,
  sp.names=c('ESG.Assets.Spread'),
  nsim=10000,
  tstep=35,
  ipy=252,
  ufr=0.042,
  epsilon=1,
  seed=2,
  useCapFloor=FALSE,
  tenors=60,
  smith.wilson.decimals=6,
  smith.wilson.curves='W:/1_Group Actuarial Function/Stochastic Scenarios/KID_PRIIPs/_Delivery/InputData/SWData_PRIIPs_Input_Template_5ysDaily_20171020.RData',
  pkg.constants=PRIIPSKIDScenarioGeneratorConstants('d:/Projects/PRIIPSKIDScenarioGenerator.config')
)
