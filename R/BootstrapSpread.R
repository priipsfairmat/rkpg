BootstrapSpread = function(spread.vec, w.stress, pkg.parameters ){
  # Returns m simulated spread level for any projection year 1, ...,hT
  #
  # Args:
  #   spread.vec: 5year historical series of spread levels (Vector[5*ipy])
  #   w.stress: size of the window to estimate the stressed volatility.
  #   pkg.parameters: arguments used in the call to the routine.
  #
  # Returns:
  #   scenarios.rn: Risk neutral simulations of spread level (matrix[hT, m])
  #   scenarios.rw: Performance simulations of spread level (matrix[hT, m])
  #   scenarios.stress: Stress simulations of spread level (matrix[hT, m])

  set.seed(pkg.parameters$seed) # set initial seed
  x = spread.vec/100*10000
  log.ret = log(x[-1]/x[-length(x)])
  log.ret = as.matrix(log.ret)
  risk.neutral.rate = 0 # flat target

  N = pkg.parameters$tstep*pkg.parameters$ipy
  year.end = seq(pkg.parameters$ipy, N, pkg.parameters$ipy)
  M0 = nrow(log.ret)
  M1 = mean(log.ret)
  M2 = sum((log.ret-M1)^2)/M0
  sigma = sqrt(M2)
  rho = 0
  sigmaccy = 0

  #MRM SCENARIOS
  scenarios.rn1 = array(1, dim=c(pkg.parameters$tstep,pkg.parameters$nsim))
  scenarios.rw1 = array(1, dim=c(pkg.parameters$tstep,pkg.parameters$nsim))
  scenarios.rw = array(1, dim=c(1,pkg.parameters$nsim*(pkg.parameters$tstep+1)))
  scenarios.rn= array(1, dim=c(1,pkg.parameters$nsim*(pkg.parameters$tstep+1)))
  scenarios.stress = array(1, dim=c(1,pkg.parameters$nsim*(pkg.parameters$tstep+1)))
  scenarios.stress1 = array(1, dim=c(pkg.parameters$tstep,pkg.parameters$nsim))
  u = round(runif(pkg.parameters$nsim*N)*(M0-1)+1)
  u = matrix(u, N, pkg.parameters$nsim)
  for (k in seq(pkg.parameters$nsim)){
    x.rn = log.ret[u[,k]]
    x.rn = cumsum(x.rn)
    x.rn = x.rn[year.end]
    scenarios.rn1[,k] = x.rn
  }

  scenarios.rn2 = scenarios.rn1 - M1*seq(pkg.parameters$tstep)*pkg.parameters$ipy +  risk.neutral.rate*seq(pkg.parameters$tstep)  - 0.5*sigma^2*seq(pkg.parameters$tstep)*pkg.parameters$ipy - rho*sigma*sigmaccy*seq(pkg.parameters$tstep)
  #scenarios.rn2 = scenarios.rn1 - apply(scenarios.rn1, 1, mean) +  risk.neutral.rate*seq(hT)  - 0.5*sigma^2*seq(hT)*ipy - rho*sigma*sigmaccy*seq(hT)
  scenarios.rn2 = exp(scenarios.rn2)

  #Performace Scenarios - ANNEX IV
  #Performace Scenarios for intermediate holding period - ANNEX IV
  #10000 stress SCENARIOS

  ws = StressedVolatility(M0, w.stress, log.ret)
  logdebug(paste("sigma (spread)",sigma*sqrt(pkg.parameters$ipy)))
  logdebug(paste("ws (spread)",ws*sqrt(pkg.parameters$ipy)))

  r.adj = log.ret*ws/sigma
  M1.str=mean(r.adj)

  for (k in seq(pkg.parameters$nsim)){
    x.stress = r.adj[u[,k],]
    x.stress = cumsum(x.stress)
    x.stress = x.stress[year.end]
    scenarios.stress1[,k] = x.stress
  }
  scenarios.stress2 = scenarios.stress1 - M1.str*seq(pkg.parameters$tstep)*pkg.parameters$ipy - 0.5*ws^2*seq(pkg.parameters$tstep)*pkg.parameters$ipy
  #scenarios.stress2 = scenarios.stress1 - apply(scenarios.stress1, 1, mean) - 0.5*ws^2*seq(hT)*ipy
  scenarios.stress2 = exp(scenarios.stress2)

  #10000 FAV, MOD, UNF SCENARIOS
  for (k in seq(pkg.parameters$nsim)){
    x.rw = log.ret[u[,k],]
    x.rw = cumsum(x.rw)
    x.rw = x.rw[year.end]
    scenarios.rw1[,k] = x.rw
  }
  scenarios.rw2 = scenarios.rw1 - 0.5*sigma^2*seq(pkg.parameters$tstep)*pkg.parameters$ipy
  #scenarios.rw2 = scenarios.rw1 - apply(scenarios.rw1, 1, mean) + sum(log.ret)*seq(hT)*ipy/length(log.ret)- 0.5*sigma^2*seq(hT)*ipy
  #scenarios.rw2 = scenarios.rw1 - apply(scenarios.rw1, 1, mean) + sum(log.ret)*seq(hT)*ipy/length(log.ret)
  scenarios.rw2 = exp(scenarios.rw2)

  result.spread = list(scenarios.rn2, scenarios.rw2, scenarios.stress2, ws)
  return(result.spread)
}
