WriteStatisticsSpread = function(sf.name,
                                 sf.vec.tr,
                                 zero.rate,
                                 initial.price.spread,
                                 spread,
                                 pkg.parameters,
                                 output.folder.S ){
  # Returns 1 CSV files reporting the main statistics regarding the simulated underlyings.
  #
  # Args:
  #   spread: Simulated risk-neutral, performance, stress scenarios of spread levels. (list[3])
  #
  # Returns:
  #   Statisticsspread.csv: Mean, target mean, volatility and 3 quantiles of spread index, computed for each of 3 scenarios

  m <- pkg.parameters$nsim
  hT <- pkg.parameters$tstep
  ipy <- pkg.parameters$ipy

  # Statistics
  num.stat = 5 # Number of statistics: mean, volatility, Q10, Q50, Q90
  stat.spread.rn  = array(1, dim = c(hT+1, num.stat))
  stat.spread.rw = array(1, dim = c(hT+1, num.stat))
  stat.spread.stress  = array(1, dim = c(hT+1, num.stat))

    for (i in seq(1,hT+1)){
      if (i==1){
        for (j in seq(num.stat)){
          if(j==2){
            stat.spread.rn[1,j]= stat.spread.rw[1,j]=stat.spread.stress[1,j]='NA'
          } else{
            stat.spread.rn[1,j]=stat.spread.rw[1,j]=stat.spread.stress[1,j]=initial.price.spread
          }
        }

      } else{
        stat.spread.rn0 = StatisticsLevel(spread[[1]], initial.price.spread, i-1, pkg.parameters)
        stat.spread.rw0= StatisticsLevel(spread[[2]], initial.price.spread, i-1, pkg.parameters)
        stat.spread.stress0 = StatisticsLevel(spread[[3]],initial.price.spread,  i-1, pkg.parameters)
        for (j in seq(num.stat)){
          stat.spread.rn[i,j] =stat.spread.rn0[[j]]
          stat.spread.rw[i,j] =stat.spread.rw0[[j]]
          stat.spread.stress[i,j] =stat.spread.stress0[[j]]
        }
      }
  }

  target.volatility = stdev.vec(sapply(sf.vec.tr, as.numeric))*sqrt(ipy)*rep(1,hT+1)
  target.volatility.stress = spread[[4]]*sqrt(ipy)*rep(1,hT+1)
  target.volatility[1]=target.volatility.stress[1]='NA'
  if (pkg.parameters$spread.model.gho) {
    target.spread.rn = initial.price.spread*(1+zero.rate[1:(hT+1)])^(seq(0,hT))
  } else {
    target.spread.rn = initial.price.spread
  }
  target.spread.rw= initial.price.spread*exp(mean(sf.vec.tr)*(seq(0,hT)*ipy)) # correct target
  #target.spread.rw= initial.price.spread*mean(exp(sf.vec[[3]]))^(seq(hT)*ipy) # correct target
  target.spread.stress = initial.price.spread*exp(rep(0, hT+1))

  stat.spread = cbind(seq(0,hT), target.spread.rn, stat.spread.rn[,1], target.volatility, stat.spread.rn[,2:5],
                      target.spread.rw, stat.spread.rw[,1], target.volatility, stat.spread.rw[,2:5],target.spread.stress,
                      stat.spread.stress[,1], target.volatility.stress, stat.spread.stress[,2:5])

  col.names <- c("TimeStep", "Target Mean - Risk Neutral","Mean - Risk Neutral", "Target Volatility - Risk Neutral", "Volatility - Risk Neutral",
                 "Q10 - Risk Neutral", "Q50 - Risk Neutral", "Q90 - Risk Neutral",
                 "Target Mean - Performance","Mean - Performance", "Target Volatility - Performance", "Volatility - Performance",
                 "Q10 - Performance", "Q50 - Performance", "Q90 - Performance",
                 "Target Mean - Stress","Mean - Stress","Target Volatility - Stress","Volatility - Stress",
                 "Q10 - Stress", "Q50 - Stress", "Q90 - Stress")
  col.names = sapply(col.names, as.character)
  colnames(stat.spread) = col.names
  filename = paste("/", "Statistics_", sf.name, ".csv", sep="")
  write.csv(stat.spread , paste(output.folder.S, filename, sep=""), row.names=F, quote = FALSE)
  }
