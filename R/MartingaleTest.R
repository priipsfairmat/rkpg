MartingaleTest <- function(pkg.parameters,
                           sf.names,
                           sp.names,
                           curve,
                           spread.mat.rn,
                           equity.mat.rn,
                           property) {


  #Calculate the Cash TR and Deflator Risk-Neutral
  #

  m <- pkg.parameters$nsim
  hT <- pkg.parameters$tstep
  ipy <- pkg.parameters$ipy
  tenors <- pkg.parameters$tenors

  y <- 0.95
  ef <- qnorm(.5+y/2)/sqrt(m)*sqrt(2)


  # Cash Total Return and Deflator

  cash.total.return.rn <- CashTotalReturn(matrix(curve[1])[[1]], pkg.parameters)
  deflator.rn = t(Deflator(cash.total.return.rn, pkg.parameters))


  #Martingale Test Cash TR (should be 1)
  cum = matrix(1, nrow = hT+1, ncol = m)
  mt = matrix(1, nrow = hT+1, ncol = m)
  for (j in 1:m){

    for (i in 1:(hT+1)){

      if(i==1){
        cum[i,j]<-1
      }else{
        cum[i,j]<-cum[i-1,j]*(1+cash.total.return.rn[i+(j-1)*(hT+1)])
      }
      mt[i,j]<-cum[i,j]*deflator.rn[i+(j-1)*(hT+1)]
    }
  }
  mt.avg.cash<-apply(mt,1,mean)



#Martingale Test Equities
mt.avg.equity = matrix(1, nrow = hT+1, ncol = dim(equity.mat.rn)[2])
for (h in 1:dim(equity.mat.rn)[2]){

  cum = matrix(1, nrow = hT+1, ncol = m)
  mt = matrix(1, nrow = hT+1, ncol = m)
  for (j in 1:m){

    for (i in 1:(hT+1)){

      if(i==1){
        cum[i,j]<-1
      }else{
        cum[i,j]<-cum[i-1,j]*(1+equity.mat.rn[,h][i+(j-1)*(hT+1)])
      }
      mt[i,j]<-cum[i,j]*deflator.rn[i+(j-1)*(hT+1)]
    }
  }
  mt.avg.equity[,h]<-apply(mt,1,mean)

}

#Martingale Test Spreads
mt.avg.spread = matrix(1, nrow = hT+1, ncol = dim(spread.mat.rn)[2])
for (h in 1:dim(spread.mat.rn)[2]){

  cum = matrix(1, nrow = hT+1, ncol = m)
  mt = matrix(1, nrow = hT+1, ncol = m)
  for (j in 1:m){

    for (i in 1:(hT+1)){

      if(i==1){
        cum[i,j]<-1
      }else{
        cum[i,j]<-cum[i-1,j]*(1+spread.mat.rn[,h][i+(j-1)*(hT+1)])
      }
      mt[i,j]<-cum[i,j]*deflator.rn[i+(j-1)*(hT+1)]
    }
  }
  mt.avg.spread[,h]<-apply(mt,1,mean)

}

# # Property, sono i Prices
 property.rn = VectorizationReturnYoY(property[[1]], pkg.parameters)

#Martingale Test Property
cum = matrix(1, nrow = hT+1, ncol = m)
mt = matrix(1, nrow = hT+1, ncol = m)
for (j in 1:m){

  for (i in 1:(hT+1)){

    if(i==1){
      cum[i,j]<-1
    }else{
      cum[i,j]<-cum[i-1,j]*(1+property.rn[i+(j-1)*(hT+1)])
    }
    mt[i,j]<-cum[i,j]*deflator.rn[i+(j-1)*(hT+1)]
  }
}
mt.avg.prop<-apply(mt,1,mean)

#construct matrix as output and write
data.mt<-cbind(c('Cash TR','Cash TR',mean(mt.avg.cash),min(mt.avg.cash),max(mt.avg.cash),'', mt.avg.cash),
               rbind(sp.names,'SPREAD',apply(mt.avg.spread,2,mean),apply(mt.avg.spread,2,min),apply(mt.avg.spread,2,max),'', mt.avg.spread),
               rbind(sf.names,'EQUITY',apply(mt.avg.equity,2,mean),apply(mt.avg.equity,2,min),apply(mt.avg.equity,2,max),'', mt.avg.equity),
               c('Property','PROPERTY',mean(mt.avg.prop),min(mt.avg.prop),max(mt.avg.prop),'', mt.avg.prop)
               )
 data.mt <- cbind(c('Description', 'Asset Type', 'Mean', 'Min', 'Max','Timestep',c(0,seq(hT))), data.mt)
logdebug(paste('Martingale test calculation complete.'))
return(data.mt)

#///da SM///
  # totret.martingaleAvg <- lapply(totret.cumulated,FUN=function(cum) {
  #   data <- cum * deflator.rn
  #   adata <- (data[,seq(1,m,2)]+data[,seq(2,m,2)]) / 2
  #   apply(adata,1,mean)
  # })
  #
  # totret.martingaleStdErr <- lapply(totret.cumulated,FUN=function(cum) {
  #   data <- t(cum) * deflator.rn
  #   adata <- (data[,seq(1,m,2)]+data[,seq(2,m,2)]) / 2
  #   apply(adata,1,sd)*ef
  # })
#////////////////////////

# #remove some variables
# rm(mt.avg.spread,
#    mt.avg.equity,
#    mt.avg.prop,
#    mt,
#    cum)
# gc()

}
