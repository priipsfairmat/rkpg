\name{PRIIPSKIDScenarioGenerator-input}
\alias{GetDataStartIndex}
\alias{GetPriceSeries}
\alias{GetSeriesReturns}
\alias{GetReturns}
\alias{GetZR}
\alias{GetSpread}
\alias{GetSpreads}
\alias{GetProperty}
\alias{GetProperties}

\title{
PRIIPSKID Scenario Generator input functions
}
\description{
Input functions for PRIIPS/KID scenario generation.
}
\usage{
GetDataStartIndex(RawData)
GetPriceSeries(IndexName, RawData, DataIndex, DescriptionIndex)
GetSeriesReturns(PriceSeries, Name)
GetReturns(SFName, XLSPath, pkg.parameters)
GetZR(XLSPath)
GetSpread(XLSPath)
GetSpreads(Name, XLSPath)
GetProperty(XLSPath)
GetProperties(Name, XLSPath)

}

\arguments{
  \item{pkg.parameters}{application parameters object}
  \item{RawData}{sheet read from input template}
  \item{IndexName}{equity index name}
  \item{DataIndex}{start row of historical data}
  \item{DescriptionIndex}{index of row with fund labels}
  \item{PriceSeries}{historical index price series}
  \item{Name}{fund or spread index name}
  \item{SFName}{fund name}
  \item{XLSPath}{input data template path}
}

\details{
Implements the PRIIPs/KID input functions.
}

\value{
GetDataStartIndex returns the position of the first row with historical data.
GetPriceSeries returns a list with
\item{Price}{historical price index}
\item{TotalReturn}{historical total returns of the equity index}
GetSeriesReturns returns the weighted returns of the fund.
GetReturns returns a list with
\item{Dates}{historical dates}
\item{Price}{relative variations of price index}
\item{TotalReturn}{relative variations of the index total return}
GetZR returns the historical  data for yield curve.
GetSpread returns the historical data for a unique spread index (labelled Credit).
GetSpreads returns the historical data for the named spread index.
GetProperty returns the property index historical data.
GetProperties returns the selected property index historical data.

}

\author{
Rodney Koren <rodney.koren@generali.com>
}

% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~internal }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~file }% __ONLY ONE__ keyword per line
